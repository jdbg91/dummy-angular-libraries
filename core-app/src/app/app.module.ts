import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TestLibraryModule } from 'test-library';
import { HeaderComponent } from './components/header/header.component';

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TestLibraryModule.forRoot({
      components: [{ name: 'CustomComponent', component: HeaderComponent }]
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
