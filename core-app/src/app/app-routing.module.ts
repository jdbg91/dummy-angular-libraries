import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { TestLibraryRoutingModule } from 'test-library';

const routes: Routes = [
  {
    path: 'custom-page',
    component: HeaderComponent
  }
];

@NgModule({
  imports: [TestLibraryRoutingModule.withRoutes(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
