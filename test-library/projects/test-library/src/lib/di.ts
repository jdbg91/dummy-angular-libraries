import { InjectionToken } from '@angular/core';
import { Config } from './types';

export const CONFIG_TOKEN = new InjectionToken<Config>('CONFIG');
