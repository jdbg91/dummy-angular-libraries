import { ModuleWithProviders, NgModule } from '@angular/core';
import { TestLibraryService } from './test-library.service';
import { CONFIG_TOKEN } from './di';
import { Config } from './types';
import { ComponentLibraryModule } from 'component-library';
import { TitleComponent } from './components/title/title.component';
import { Page1Component } from './pages/page1/page1.component';
import { Page2Component } from './pages/page2/page2.component';
import { TestLibraryRoutingModule } from './test-library-routing.module';

@NgModule({
  declarations: [
    TitleComponent,
    Page1Component,
    Page2Component
  ],
  imports: [
    ComponentLibraryModule,
    TestLibraryRoutingModule
  ],
  providers: [TestLibraryService],
  exports: [ TitleComponent, Page1Component, Page2Component]
})
export class TestLibraryModule {
  static forRoot(config: Config): ModuleWithProviders<TestLibraryModule> {
    return {
      ngModule: TestLibraryModule,
      providers: [{ provide: CONFIG_TOKEN, useValue: config }]
    };
  }
}
