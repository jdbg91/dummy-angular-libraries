import {
  ComponentFactoryResolver,
  ComponentRef,
  Inject,
  Injectable,
  Optional,
  ViewContainerRef
} from '@angular/core';
import { ComponentConfig, Config } from './types';
import { CONFIG_TOKEN } from './di';
import { ButtonComponent } from 'component-library';
import { TitleComponent } from './components/title/title.component';

@Injectable()
export class TestLibraryService {
  components: Array<ComponentConfig> = [
    { name: 'Title', component: TitleComponent },
    { name: 'Button', component: ButtonComponent }
  ];
  componentRef: ComponentRef<any> | undefined;

  constructor(
    @Optional() @Inject(CONFIG_TOKEN) private config: Config | null,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    if (config?.components) {
      this.components.push(...config.components);
    }
  }

  render(
    componentName: string,
    viewContainerRef: ViewContainerRef | undefined
  ): void {
    const component = this.getComponentByName(componentName);
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      component
    );
    this.componentRef = viewContainerRef?.createComponent(componentFactory);
  }

  getComponentByName(componentName: string): any {
    return this.components.find(component => component.name === componentName)
      ?.component;
  }
}
