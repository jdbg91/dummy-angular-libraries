export interface Config {
  components: Array<ComponentConfig>;
}

export interface ComponentConfig {
  name: string;
  component: any;
}
