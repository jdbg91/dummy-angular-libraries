import {
  AfterViewInit,
  Component,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { TestLibraryService } from '../../test-library.service';

@Component({
  selector: 'lib-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements AfterViewInit {
  @ViewChild('anchor', { read: ViewContainerRef }) anchorPoint:
    | ViewContainerRef
    | undefined;
  pageComponents: string[] = ['Title', 'CustomComponent'];

  constructor(private testLibraryService: TestLibraryService) {}

  ngAfterViewInit(): void {
    this.pageComponents.forEach(pageComponent =>
      this.testLibraryService.render(pageComponent, this.anchorPoint)
    );
  }
}
