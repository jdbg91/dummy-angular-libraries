import {
  AfterViewInit,
  Component,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { TestLibraryService } from '../../test-library.service';

@Component({
  selector: 'lib-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements AfterViewInit {
  @ViewChild('anchor', { read: ViewContainerRef }) anchorPoint:
    | ViewContainerRef
    | undefined;
  pageComponents: string[] = ['Title', 'Button'];

  constructor(private testLibraryService: TestLibraryService) {}

  ngAfterViewInit(): void {
    this.pageComponents.forEach(pageComponent =>
      this.testLibraryService.render(pageComponent, this.anchorPoint)
    );
  }
}
